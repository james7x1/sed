% Rotate from ecliptic to BCRS coordinates. */

   x = ph[0];
   y = ph[1];
   z = ph[2];
   pvh[0][0] =      x + am12*y + am13*z;
   pvh[0][1] = am21*x + am22*y + am23*z;
   pvh[0][2] =          am32*y + am33*z;

   x = vh[0];
   y = vh[1];
   z = vh[2];
   pvh[1][0] =      x + am12*y + am13*z;
   pvh[1][1] = am21*x + am22*y + am23*z;
   pvh[1][2] =          am32*y + am33*z;

   x = pb[0];
   y = pb[1];
   z = pb[2];
   pvb[0][0] =      x + am12*y + am13*z;
   pvb[0][1] = am21*x + am22*y + am23*z;
   pvb[0][2] =          am32*y + am33*z;

   x = vb[0];
   y = vb[1];
   z = vb[2];
   pvb[1][0] =      x + am12*y + am13*z;
   pvb[1][1] = am21*x + am22*y + am23*z;
   pvb[1][2] =          am32*y + am33*z;
