#!/bin/bash
#Parse C Multi-dimensional Matrices

sed 's/\},$/;.../g' dtdb_coe.txt > dtdb_coe1.txt #Replace }, with end row and continue on next line
sed -i 's/\}$/\];/g' dtdb_coe1.txt #Replace last } with a ];
sed -i '/\};$/d' dtdb_coe1.txt #Delete lines that are exactly };
sed -i 's/static const double fairhd\[787\]\[3\] = {/fairhd \= \[.../g' dtdb_coe1.txt #Fix first line
sed -i '/\/\*/d' dtdb_coe1.txt #Delete comment lines
sed -i '/^\s*$/d' dtdb_coe1.txt #Delete empty lines
sed -i 's/{//g' dtdb_coe1.txt #Remove opening brackets
