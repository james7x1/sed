#!/bin/bash
#Parse Complex Float MATLAB vector into arma cx_fvec

# Pattern for Real part
real="[0-9]+\.[0-9]+\s" # Match float followed by a space of each line
imag="\W\s[0-9]+\.[0-9]+i" # Match non-word, space, then float, ending in i
sed "s|^|std::complex<float>(|" MATLAB_complex.txt > MATLAB_complex1.txt # Add std::complex<float>( to each beginning of line
sed -i "1 s|^|{|" MATLAB_complex1.txt # Add { to beginning of file
sed -i "s|^s| &|" MATLAB_complex1.txt # Add space to beginning of all lines past the first one to align columns
sed -i "s|\([0-9]\)\s|\1\,|" MATLAB_complex1.txt # Replace first space with comma
sed -i "s|i$|),|" MATLAB_complex1.txt # Replaces i at end of line with closing parenthese and comma
sed -i " $ s|,$|}|" MATLAB_complex1.txt # Replaces comma at end of the last line with a bracket