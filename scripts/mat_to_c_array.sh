#!/bin/bash
#Parse Integer MATLAB Matricies into C Matrices

sed 's|\([0-9]\)\s|\1,|g' ../data/array.txt > array1.txt # Add a comma between elements
sed -i 's|\([0-9]\)$|\1,|g' array1.txt # Add comma after numbers at the end of lines
