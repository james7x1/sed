#!/bin/bash
#Change Dates in STARLINK Ephemeris file

path='MEME_52880_STARLINK-4186_2720811_Operational_1380269520_UNCLASSIFIED'
increment='1'
src="${path}.txt"
new_path="${path}${increment}.txt"

sed 's/2023272/1992010/g' ${src} > ${new_path} #Replace Date with desired date for 1st day
sed -i 's/2023273/1992011/g' ${new_path} #Replace Date with desired date for 2nd day
sed -i 's/2023274/1992012/g' ${new_path} #Replace Date with desired date for 3rd day
sed -i 's/2023275/1992013/g' ${new_path} #Replace Date with desired date for 4th day

