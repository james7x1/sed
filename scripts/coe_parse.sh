#!/bin/bash
#Parse C Matrices

sed 's/[0-9]\{2\},$/&.../g' coe.txt > coe1.txt #Add '...' to end of each line that ends with a comma
sed -i '/^\s*$/d' coe1.txt
sed -i 's/\[\] = {/ = \[.../g' coe1.txt
sed -i 's/static.*double //g' coe1.txt
sed -i 's/ \}/]/g' coe1.txt
sed -i 's|\/\*|%|g' coe1.txt
