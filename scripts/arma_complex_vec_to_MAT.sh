#!/bin/bash
#Parse amra cx_fvec to Complex Float MATLAB vector

# Pattern for Real part
sed "s|^(||" arma_cx_fvec.txt > arma_cx_fvec1.txt # Remove opening parenthese at beginning of line
sed -i "s|)$|i,|" arma_cx_fvec1.txt # Remove closing parenthese at end of line
sed -i "s|\,||" arma_cx_fvec1.txt
sed -i "1 s|^|[|" arma_cx_fvec1.txt
sed -i "$ s|,$|]|" arma_cx_fvec1.txt
sed -i "s|,$|,...|" arma_cx_fvec1.txt