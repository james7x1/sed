Repository to hold a history of my SED scripts.
This is to help me learn SED quicker and remember tricks I learn as I go.
This is particularly useful when I don't use it very often and pick it up again in a critical moment.

Structure:
scripts: Contains SED scripts
data: Contains RAW data before running script when available
